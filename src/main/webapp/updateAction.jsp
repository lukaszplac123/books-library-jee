<%@page import="book.model.BookRepository"%>
<%@page import="java.util.List"%>
<%@page import="book.model.Book"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ taglib prefix="mfn" uri="/WEB-INF/custom-tags.tld"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<body>

<c:set value="${param.updateBook}" var="confirmation"></c:set>
<c:set value="${param.update}" var="update" scope="page"></c:set>
<c:set value="${applicationScope.booksList[update]}" var="bookToUpdate"/>
<p><c:out value="${bookToUpdate.bookName}"/></p>
<c:choose>
	<c:when test="${confirmation=='Confirm' && (requestScope['validData'] != 'no')}">
		<jsp:useBean id="newBook" class="book.model.Book">
			<jsp:setProperty property="bookName" value="${param.bookName}" name="newBook"/>
			<jsp:setProperty property="bookAuthor" value="${param.bookAuthor}" name="newBook"/>
			<jsp:setProperty property="numberOfPages" value="${param.numberOfPages}" name="newBook"/>
			<jsp:setProperty property="date" value="${requestScope['dateConverted']}" name="newBook"/>
		</jsp:useBean>
			<% 
				BookRepository repository = (BookRepository) application.getAttribute("booksRepo");
				int index = Integer.parseInt((String) request.getAttribute("update"));
				repository.updateBook(index, newBook);
			%>
		<p>Book updated. Proceed or get back to the list</p>
	</c:when>
	<c:when test="${requestScope['validData'] == 'no'}">
		<p>Wrong data provided. Try again.</p>
	</c:when>
	<c:otherwise>
		<p>Please update a Book:</p>
	</c:otherwise>
</c:choose>

<form action="BookServlet" method="POST">
	Book[Title]: <input type="text" name="bookName" value="${bookToUpdate.bookName}"><br>
	Author: <input type="text" name ="bookAuthor" value="${bookToUpdate.bookAuthor}"><br>
	No. of pages: <input type="number" name ="numberOfPages" value="${bookToUpdate.numberOfPages}"><br>
	Date of publishing: <input type="text" name ="date" value="${bookToUpdate.date}"><br>
	<input type="hidden" name ="update" value="${update}"><br>
	<input type="submit" value="Confirm" name ="updateBook"><br>
	<input type="submit" value="Back" name = "submitBack"></button>
</form>
</body>
</html>
