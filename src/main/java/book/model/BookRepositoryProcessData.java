package book.model;

import java.io.IOException;
import java.net.HttpRetryException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/BookRepositoryProcessData")
public class BookRepositoryProcessData extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processData(req);
		Map<String, String[]> paramMap = (Map<String, String[]>) req.getAttribute("paramMap");
		if (paramMap.containsKey("submitNewBook")){
		req.getRequestDispatcher("addAction.jsp").forward(req, resp);
		} else{
			req.setAttribute("update", req.getParameter("update"));
			req.getRequestDispatcher("updateAction.jsp").forward(req, resp);	
		}
	}

	private void processData(HttpServletRequest req) {
		String str = req.getParameter("bookName");
		if (str != ""){
			String date = req.getParameter("date");
			try{
			LocalDate dateConverted = formatDateFromStringToLocalDate(date);
			req.setAttribute("dateConverted", dateConverted);
			} catch(DateTimeParseException ex){
				req.setAttribute("validData", "no");
			}
		} else{
			req.setAttribute("validData", "no");
		}
	}
	
	private LocalDate formatDateFromStringToLocalDate(String date) throws DateTimeParseException{
		CharSequence cs = date;
		LocalDate dateConverted;
		dateConverted = LocalDate.parse(cs);
		return dateConverted;
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
	
}
