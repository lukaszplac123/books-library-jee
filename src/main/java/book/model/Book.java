package book.model;

import java.time.LocalDate;
import java.util.Date;

public class Book {
	
	private String bookName;
	private String bookAuthor;
	private int numberOfPages;
	private LocalDate date;
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getBookAuthor() {
		return bookAuthor;
	}
	public void setBookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}
	public int getNumberOfPages() {
		return numberOfPages;
	}
	public void setNumberOfPages(int numberOfPages) {
		this.numberOfPages = numberOfPages;
	}
	
	public Book(){}
	
	public Book(String bookName, String bookAuthor, int numbeOfPages, LocalDate date){
		this.bookName = bookName;
		this.bookAuthor = bookAuthor;
		this.numberOfPages = numbeOfPages;
		this.date = date;
	}
}
