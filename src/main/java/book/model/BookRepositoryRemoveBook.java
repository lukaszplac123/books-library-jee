package book.model;

import java.io.IOException;
import java.net.HttpRetryException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import book.controller.BookServlet;

@WebServlet("/BookRepositoryRemoveBook")
public class BookRepositoryRemoveBook extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String indexAsText = req.getParameter("remove");
		List<Book> booksList = (List<Book>) getServletContext().getAttribute("booksList");
		booksList.remove(Integer.parseInt(indexAsText));
		req.getRequestDispatcher("booksTable.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
		
}
