package book.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BookRepository {

	private List<Book> booksList;
	
	public List<Book> getBooksList() {
		return booksList;
	}
	
	public BookRepository() {
		List<Book> bookRepoTemp = new ArrayList<Book>();
		booksList = Collections.synchronizedList(bookRepoTemp);
	}
	
	public void addBook(Book book){
		booksList.add(book);
	}
	public void updateBook(int index, Book bookData){
		Book book = booksList.get(index);
		book.setBookName(bookData.getBookName());
		book.setBookAuthor(bookData.getBookAuthor());
		book.setNumberOfPages(bookData.getNumberOfPages());
		book.setDate(bookData.getDate());
	}
}
