package book.controller;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import book.model.Book;
import book.model.BookRepository;

public class BookRepositoryListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		BookRepository bookRepository = new BookRepository();
		Book tempBook1 = new Book("Quo Vadis", "Sienkiewicz", 200, LocalDate.of(2001, 11, 15));
		Book tempBook2 = new Book("Dziady III", "Mickiewicz", 200, LocalDate.of(2001, 11, 16));
		bookRepository.addBook(tempBook1);
		bookRepository.addBook(tempBook2);
		sce.getServletContext().setAttribute("booksRepo", bookRepository);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
	}

}
