package book.controller;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import book.model.Book;
import book.model.BookRepository;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/BookServlet")
public class BookServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public BookServlet() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		Map<String, String[]> paramMap = request.getParameterMap();
		request.setAttribute("paramMap", paramMap);
		if (paramMap.containsKey("add")){
			response.sendRedirect("addAction.jsp");
		} else if (paramMap.containsKey("remove")){
			request.getRequestDispatcher("BookRepositoryRemoveBook").forward(request, response);;
		} else if (paramMap.containsKey("submitNewBook") || paramMap.containsKey("updateBook")) {
			request.getRequestDispatcher("BookRepositoryProcessData").forward(request, response);
		} else if (paramMap.containsKey("submitBack")){
			response.sendRedirect("booksTable.jsp");
		} else{
			response.sendRedirect("booksTable.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
